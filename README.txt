The annotations for the QUAERO project are now available at https://github.com/ax-le/QUAERO_project_mirror.

This project was conducted by F. Bimbot et al. [1], and the repositories are intended not to lose the content of the project.

[1] Bimbot, F., Sargent, G., Deruty, E., Guichaoua, C., & Vincent, E. (2014, January). Semiotic description of music structure: An introduction to the Quaero/Metiss structural annotations. In AES 53rd International Conference on Semantic Audio (pp. P1-1).
